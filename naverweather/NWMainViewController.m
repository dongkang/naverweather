//
//  NWMainViewController.m
//  naverweather
//
//  Created by 강동혁 on 2014. 5. 10..
//  Copyright (c) 2014년 dongkang. All rights reserved.
//

#import "NWMainViewController.h"

@interface NWMainViewController ()

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation NWMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.webView];
    [self loadWeather];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Properties

- (UIWebView *)webView {
    
    if(_webView == nil) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width, self.view.bounds.size.height - 20)];
        _webView.autoresizesSubviews = true;
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return _webView;
}

- (void)loadWeather {
    NSURL *url = [NSURL URLWithString:@"http://m.weather.naver.com/m/main.nhn"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

@end
