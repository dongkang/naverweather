//
//  NWMainViewController.h
//  naverweather
//
//  Created by 강동혁 on 2014. 5. 10..
//  Copyright (c) 2014년 dongkang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NWMainViewController : UIViewController

- (void)loadWeather;

@end
