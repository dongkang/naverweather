//
//  main.m
//  naverweather
//
//  Created by 강동혁 on 2014. 5. 10..
//  Copyright (c) 2014년 dongkang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NWAppDelegate class]));
    }
}
