//
//  NWAppDelegate.h
//  naverweather
//
//  Created by 강동혁 on 2014. 5. 10..
//  Copyright (c) 2014년 dongkang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NWMainViewController.h"

@interface NWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NWMainViewController *mainViewController;

@end
